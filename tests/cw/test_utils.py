from collections import Hashable

from cw.utils import _freeze, dedupe_list


def test__freeze():
    assert isinstance(_freeze(dict()), Hashable)
    assert isinstance(_freeze(set()), Hashable)
    assert isinstance(_freeze(tuple()), Hashable)
    assert isinstance(_freeze(list()), Hashable)
    assert isinstance(_freeze(str()), Hashable)


def test_dedupe_list():
    item = [1, 2, 3, 2, 4, 1, 5, 6, 2]
    assert dedupe_list(item) == [1, 2, 3, 4, 5, 6]
