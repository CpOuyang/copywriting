from cw.model import (
    remove_hyper_tags,
    score_entities,
    lookup_profiles,
)


def test_remove_hyper_tags():
    raw_text = """
    <a href="https://developer.org/" target="_blank">
    Foo
    </a>
    """
    assert remove_hyper_tags(raw_text).strip() == 'Foo'


def test_score_entities():
    raw_text = "一家烤肉萬家香"
    book = [
        {'alias': '萬家香',
         'entities': ['烤肉'], }
    ]
    scored = score_entities(
        raw_text,
        book,
    )
    assert isinstance(scored, list)
    assert isinstance(scored[0], dict)
    assert scored[0].get('index') == 0
    assert scored[0].get('score') == 11
    assert scored[0].get('alias') == '萬家香'
    assert scored[0].get('matches') == ['萬家香', '烤肉']


def test_lookup_profiles():
    alias = '萬家香'
    book = [
        {'alias': '萬家香',
         'type': '零售',
         'subtype': '食品', }
    ]
    looked = lookup_profiles(
        alias,
        book,
    )
    assert looked == book[0]
