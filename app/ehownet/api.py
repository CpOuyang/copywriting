from typing import Text, List, Dict, Any
import logging

from flask import Flask, request, jsonify

from ehownet.ehownet_python3 import WordNode, SemanticTypeNode, EHowNetTree
from ehownet.handler import path_db, Handler, WordHandler, SemanticHandler, TreeHandler

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False


def _get_handlers_attributes(handlers: List[Handler],
                             return_type: Text,
                             *wrappers: Any):
    """Pseudo code:
        return [ *wrappers( handler.<name|arch>, on return_type ) for handler in handlers ]
    """
    answer = []
    for handler in handlers:
        wrapped = handler
        if wrappers:
            for wrapper in wrappers:
                wrapped = wrapper(wrapped)
        if return_type == 'name':
            answer.append(wrapped.name)
        elif return_type == 'arch':
            answer.append(wrapped.arch)
    return answer


@app.route('/', methods=['GET', 'POST'])
def root():
    return 'Hello World!!\n'


@app.route('/word/search/<text>', methods=['GET', 'POST'])
def word_search(text: Text):
    """
    :method: like
    :return_type: name|arch
    """
    body = request.json

    like = False
    return_type = 'arch'
    if body:
        if body.get('method') == 'like':
            like = True
        if body.get('return_type') == 'name':
            return_type = 'name'

    th = TreeHandler(EHowNetTree(path_db))
    searched = th.search_word(text, like=like)
    # The searched is a list of WordNode,
    # which needs a wrapper, WordHandler, to get attributes (of wrapper)
    answer = _get_handlers_attributes(searched, return_type, WordHandler)
    return jsonify(answer)


@app.route('/word/synonyms/<name>', methods=['GET', 'POST'])
def word_synonyms(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    wn = th.word(name)
    if wn is None:
        return 'Unknown name of WordNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    wh = WordHandler(wn)
    answer = _get_handlers_attributes(wh.synonyms, return_type)
    return jsonify(answer)


@app.route('/word/siblings/<name>', methods=['GET', 'POST'])
def word_siblings(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    wn = th.word(name)
    if wn is None:
        return 'Unknown name of WordNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    wh = WordHandler(wn)
    answer = _get_handlers_attributes(wh.siblings, return_type)
    return jsonify(answer)


@app.route('/word/descendants/<name>', methods=['GET', 'POST'])
def word_descendants(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    wn = th.word(name)
    if wn is None:
        return 'Unknown name of WordNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    wh = WordHandler(wn)
    answer = _get_handlers_attributes(wh.descendants, return_type)
    return jsonify(answer)


@app.route('/word/semantics/<name>', methods=['GET', 'POST'])
def word_semantics(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    wn = th.word(name)
    if wn is None:
        return 'Unknown name of WordNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    wh = WordHandler(wn)
    answer = _get_handlers_attributes(wh.semantics, return_type)
    return jsonify(answer)


@app.route('/semantic/search/<text>', methods=['GET', 'POST'])
def semantic_search(text: Text):
    """
    :method: like
    :return_type: name|arch
    """
    body = request.json

    like = False
    return_type = 'arch'
    if body:
        if body.get('method') == 'like':
            like = True
        if body.get('return_type') == 'name':
            return_type = 'name'

    th = TreeHandler(EHowNetTree(path_db))
    searched = th.search_semantic(text, like=like)
    # The searched is a list of SemanticTypeNode,
    # which needs a wrapper, SemanticHandler, to get attributes (of wrapper)
    answer = _get_handlers_attributes(searched, return_type, SemanticHandler)
    return jsonify(answer)


@app.route('/semantic/ancestors/<name>', methods=['GET', 'POST'])
def semantic_ancestors(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    sn = th.semantic(name)
    if sn is None:
        return 'Unknown name of SemanticTypeNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    sh = SemanticHandler(sn)
    answer = _get_handlers_attributes(sh.ancestors, return_type)
    return jsonify(answer)


@app.route('/semantic/descendants/<name>', methods=['GET', 'POST'])
def semantic_descendants(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    sn = th.semantic(name)
    if sn is None:
        return 'Unknown name of SemanticTypeNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    sh = SemanticHandler(sn)
    answer = _get_handlers_attributes(sh.descendants, return_type)
    return jsonify(answer)


@app.route('/semantic/hypernym/<name>', methods=['GET', 'POST'])
def semantic_hypernym(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    sn = th.semantic(name)
    if sn is None:
        return 'Unknown name of SemanticTypeNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    sh = SemanticHandler(sn)
    # Note the first argument
    answer = _get_handlers_attributes([sh.hypernym], return_type)
    return jsonify(answer)


@app.route('/semantic/hyponyms/<name>', methods=['GET', 'POST'])
def semantic_hyponyms(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    sn = th.semantic(name)
    if sn is None:
        return 'Unknown name of SemanticTypeNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    sh = SemanticHandler(sn)
    answer = _get_handlers_attributes(sh.hyponyms, return_type)
    return jsonify(answer)


@app.route('/semantic/words/<name>', methods=['GET', 'POST'])
def semantic_words(name: Text):
    """
    :return_type: name|arch
    """
    body = request.json

    th = TreeHandler(EHowNetTree(path_db))
    sn = th.semantic(name)
    if sn is None:
        return 'Unknown name of SemanticTypeNode.', 400

    return_type = 'arch'
    if body:
        if body.get('return_type') == 'name':
            return_type = 'name'

    sh = SemanticHandler(sn)
    answer = _get_handlers_attributes(sh.words, return_type)
    return jsonify(answer)


if __name__ == '__main__':

    app.run(host='0.0.0.0', port=5000)
