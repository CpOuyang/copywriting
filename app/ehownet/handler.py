from typing import Text, List, Dict, Optional
import os
import re
import sqlite3

from ehownet.ehownet_python3 import Node, SemanticTypeNode, WordNode, EHowNetTree

path_db = os.path.join('db', 'ehownet_ontology.sqlite')
path_db_sim = os.path.join('db', 'ehownet_ontology_sim.sqlite')


def TableGenerator(script: Text = '', path_db: Text = path_db):
    conn = sqlite3.connect(path_db)
    curs = conn.cursor()
    sql = script
    for row in curs.execute(sql):
        yield row


ehownet_tree_generator = TableGenerator('select * from tree')
ehownet_semantic_generator = TableGenerator('select * from semanticTypeNode')
ehownet_word_generator = TableGenerator('select * from wordNode')


def _remove_outer(string: Text,
                  prefix: Text = '',
                  suffix: Text = '') -> Text:
    """Remove the outermost prefix and suffix,
    in re patterns, if searched, from a string.

    Note that it is NOT allowed to define a pattern (parenthesis),
    in prefix and suffix.
    """
    pre, suf = prefix, suffix
    pattern = f'^({pre})(.+)({suf})$'
    searched = re.search(pattern, string)
    if searched is not None:
        return searched.group(2)
    return string


class Handler(object):

    def __repr__(self):
        if hasattr(self, 'name'):
            return type(self).__name__ + '(' + repr(self.name) + ')'
        return super().__repr__()


class WordHandler(Handler):

    def __init__(self, word_node: WordNode):
        self._word = word_node

    @property
    def name(self) -> Text:
        """Not to use the default method is to return originally if typo.
        """
        # return self._word.data.get('name')
        ugly = repr(self._word)
        refined = _remove_outer(ugly, prefix='word')
        refined = _remove_outer(refined, '\(', '\)')
        refined = _remove_outer(refined, '[\'"]', '[\'"]')
        return refined

    @property
    def arch(self) -> Dict:
        ms = self.name
        split = ms.split('.')
        string = pos = index = None
        if len(split) == 2:
            string, pos = split
        elif len(split) == 3:
            string, pos, index = split
        return {
            'text': string,
            'pos': pos.split(','),
            'index': index,
        }

    @property
    def synonyms(self) -> List['WordHandler']:
        answer = []
        lst = self._word.getSynonymWordList()
        for item in lst:
            wh = WordHandler(item)
            answer.append(wh)
        return answer

    @property
    def siblings(self) -> List['WordHandler']:
        answer = []
        lst = self._word.getSiblingWordList()
        for item in lst:
            wh = WordHandler(item)
            answer.append(wh)
        return answer

    @property
    def descendants(self) -> List['WordHandler']:
        answer = []
        lst = self._word.getDescendantWordList()
        for item in lst:
            wh = WordHandler(item)
            answer.append(wh)
        return answer

    @property
    def semantics(self) -> List['SemanticHandler']:
        answer = []
        lst = self._word.getSemanticTypeList()
        for item in lst:
            sem = SemanticHandler(item)
            answer.append(sem)
        return answer


class SemanticHandler(Handler):

    def __init__(self, sem_node: SemanticTypeNode):
        self._semantic = sem_node

    @property
    def name(self) -> Text:
        """Not to use the default method is to return originally if typo.
        """
        # return self._semantic.data.get('name')
        ugly = repr(self._semantic)
        refined = _remove_outer(ugly, prefix='semanticType')
        refined = _remove_outer(refined, '\(', '\)')
        refined = _remove_outer(refined, '[\'"]', '[\'"]')
        return refined

    @property
    def arch(self) -> Dict:
        ms = self.name
        split = ms.split('.')
        string = index = None
        if len(split) == 1:
            string = split[0]
        elif len(split) == 2:
            string, index = split
        return {
            'text': string,
            'index': index,
        }

    @property
    def ancestors(self) -> List['SemanticHandler']:
        answer = []
        lst = self._semantic.getAncestorList()
        for item in lst:
            sem = SemanticHandler(item)
            answer.append(sem)
        return answer

    @property
    def descendants(self) -> List['SemanticHandler']:
        answer = []
        lst = self._semantic.getDescendantList()
        for item in lst:
            sem = SemanticHandler(item)
            answer.append(sem)
        return answer

    @property
    def hypernym(self) -> Optional['SemanticHandler']:
        sem = self._semantic.getHypernym()
        if sem is not None:
            return SemanticHandler(sem)
        return None

    @property
    def hyponyms(self) -> List['SemanticHandler']:
        answer = []
        lst = self._semantic.getHyponymList()
        for item in lst:
            sem = SemanticHandler(item)
            answer.append(sem)
        return answer

    @property
    def words(self) -> List['WordHandler']:
        answer = []
        lst = self._semantic.getWordList()
        for item in lst:
            word = WordHandler(item)
            answer.append(word)
        return answer


class TreeHandler(Handler):
    """The original code lacks of something, hence enhanced.
    """
    def __init__(self, tree: EHowNetTree):
        self._tree = tree

    def search_word(self,
                    string: Text,
                    like: bool = False) -> List[WordNode]:
        """If like operator would like to be used,
        the percent sign (%) should be enclosed in the string argument.
        """
        answer = []
        condition = 'word like ?' if like else 'word = ?'
        sql = f'select * from wordNode where {condition}'
        self._tree.cursor.execute(sql, (string,))
        for row in self._tree.cursor:
            answer.append(WordNode(self, row))
        return answer

    def search_semantic(self,
                        string: Text,
                        like: bool = False) -> List[SemanticTypeNode]:
        """If like operator would like to be used,
        the percent sign (%) should be enclosed in the string argument.
        """
        answer = []
        condition = 'label like ?' if like else 'label = ?'
        sql = f'select * from semanticTypeNode where {condition}'
        self._tree.cursor.execute(sql, (string,))
        for row in self._tree.cursor:
            answer.append(SemanticTypeNode(self, row))
        return answer

    def word(self, name: Text) -> WordNode:
        """Must be the exactly name. e.g., self.word('開心.Nv,VH.1')
        """
        return self._tree.word(name)

    def semantic(self, name: Text) -> SemanticTypeNode:
        """Must be the exactly name. e.g., self.semantic('joyful|喜悅.1')
        """
        return self._tree.semanticType(name)
