import logging

from flask import Flask, request, jsonify

from ckiptrans.handler import CKIPTransformerHandler

logging.basicConfig(level=logging.DEBUG)
app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
ckiptrans = CKIPTransformerHandler()


@app.route('/', methods=['GET', 'POST'])
def root():
    return 'Hello World!!\n'


@app.route('/ws', methods=['POST'])
def post_ws():
    texts = request.json.get('texts')
    if texts is None:
        return jsonify({'Error': 'Parsing texts not found.'}), 400
    return jsonify(ckiptrans.ws(texts)), 200


@app.route('/pos', methods=['POST'])
def post_pos():
    texts = request.json.get('texts')
    if texts is None:
        return jsonify({'Error': 'Parsing texts not found.'}), 400
    return jsonify(ckiptrans.pos(texts)), 200


@app.route('/ner', methods=['POST'])
def post_ner():
    texts = request.json.get('texts')
    if texts is None:
        return jsonify({'Error': 'Parsing texts not found.'}), 400
    return jsonify(ckiptrans.ner(texts)), 200


@app.route('/parse', methods=['POST'])
def post_parse():
    texts = request.json.get('texts')
    if texts is None:
        return jsonify({'Error': 'Parsing texts not found.'}), 400
    return jsonify(ckiptrans.parse(texts)), 200


if __name__ == '__main__':

    app.run(host='0.0.0.0', port=5000)
