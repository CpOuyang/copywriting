from typing import Text, List, Dict
import logging
import os

from ckip_transformers.nlp import CkipWordSegmenter, CkipPosTagger, CkipNerChunker
from ckip_transformers.nlp.util import NerToken
from tqdm import tqdm

logging.basicConfig(level=logging.DEBUG)
MODEL_PREFIX = 'albert-base-chinese'
MODEL_FOLDER = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    'model',)
TQDM_FORMAT = '{l_bar}{bar:36}{r_bar}{bar:-10b}'


class CKIPTransformerHandler(object):
 
    def __init__(self,
                 prefix: Text = MODEL_PREFIX,
                 level: int = 3):
        self._driver = {}
        tq = tqdm(total=3, bar_format=TQDM_FORMAT)
        packages = [CkipWordSegmenter, CkipPosTagger, CkipNerChunker]
        for idx, tag in enumerate(['ws', 'pos', 'ner']):
            model_name = '{0}-{1}'.format(prefix, tag)
            tq.set_description(desc='Installing {}'.format(tag))
            tq.refresh()
            self._driver[tag] = packages[idx](
                model_name=os.path.join(MODEL_FOLDER, model_name))
            tq.update()
        tq.set_description(desc='Drivers installed')
        tq.close()
        self._logger = logging.getLogger(__name__)
        self._ws = None
        self._pos = None
        self._ner = None

    def ws(self,
           texts: List[Text],
           show_progress: bool = False) -> List[List[Text]]:
        self._ws = self._driver['ws'](texts, show_progress=show_progress)
        return self._ws

    def pos(self,
            texts: List[Text],
            show_progress: bool = False) -> List[List[Text]]:
        self._pos = self._driver['pos'](texts, show_progress=show_progress)
        return self._pos

    def ner(self,
            texts: List[Text],
            show_progress: bool = False) -> List[List[NerToken]]:
        self._ner = self._driver['ner'](texts, show_progress=show_progress)
        return self._ner

    def parse(self, texts: List[Text]) -> Dict:
        answer = {}
        answer['ws'] = self.ws(texts)
        answer['pos'] = self.pos(texts)
        answer['ner'] = self.ner(texts)
        return answer


if __name__ == '__main__':

    import pprint as pp

    texts = [
        '台積電傳出一口氣開除7名員工，',
        '由於一批人遭開除極為罕見，',
        '可能為首例，',
        '引起半導體供應鏈關注。',
        '盛傳其中與外洩客戶訂單商機有關，',
        '台積電證實解雇7名員工，',
        '但否認業界傳言，',
        '表示遭解雇的人員於在職期間嚴重違反工作規範，',
        '因情節重大，',
        '予以相應處分。',
    ]
    hh = CKIPTransformerHandler()
    pp.pprint(hh.parse(texts), width=100, compact=True, sort_dicts=False)
