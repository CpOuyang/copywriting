from ast import literal_eval
from datetime import datetime
from typing import Any, Text
from zipfile import ZipFile
import argparse
import logging
import os
if os.name == 'posix':
    import readline # necessary for input()
import shlex
import subprocess

from rich import print
from rich.console import Console

from cw.adapter import ConfigAdapter
from cw.adapter import EntityConfigAdapter, ProfileConfigAdapter
from cw.adapter import PATH_ENTITIES, PATH_PROFILES

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

MSG_HOWTO_START = 'Use "entity", "profile" or "backup" as starting argument.'
MSG_WRONG_ADAPTER = 'Improper adapter. {} is not applicable for this method.'

SYNTAX_HELP = """
    [b][u]ent[/u]ity|[u]pro[/u]file[/b] [u]app[/u]end alias value [value ...]

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] pop alias value [value ...]

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] sort alias [--dedupe]

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] mod alias attrib value

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] create alias [alias ...]

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] revoke alias [alias ...]

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] reorg 'dict-repr'

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] [u]comp[/u]are

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] show glob [glob ...] [--detailed] [--pbcopy] [--sort]

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] clear

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] update

    [b][u]ent[/u]ity|[u]pro[/u]file[/b] output \[path]
"""

entities = EntityConfigAdapter()
profiles = ProfileConfigAdapter()

parser = argparse.ArgumentParser()
parser.add_argument('method', type=str, choices=['config', 'verify'])

parser_append = argparse.ArgumentParser(prog='append')
parser_append.add_argument('alias', type=str)
parser_append.add_argument('values', type=str, nargs='+')

parser_pop = argparse.ArgumentParser(prog='pop')
parser_pop.add_argument('alias', type=str)
parser_pop.add_argument('values', type=str, nargs='+')

parser_sort = argparse.ArgumentParser(prog='sort')
parser_sort.add_argument('aliases', type=str, nargs='+')
parser_sort.add_argument('-d', '--dedupe', '--deduped', action='store_true')

parser_mod = argparse.ArgumentParser(prog='mod')
parser_mod.add_argument('alias', type=str)
parser_mod.add_argument('attrib', type=str)
parser_mod.add_argument('value', type=str)

parser_create = argparse.ArgumentParser(prog='create')
parser_create.add_argument('aliases', type=str, nargs='+')

parser_revoke = argparse.ArgumentParser(prog='revoke')
parser_revoke.add_argument('aliases', type=str, nargs='+')

parser_reorg = argparse.ArgumentParser(prog='reorg')
parser_reorg.add_argument('repr', type=str, nargs='?')

parser_show = argparse.ArgumentParser(prog='show')
parser_show.add_argument('glob', type=str, nargs='*')
parser_show.add_argument('-d', '--detail', '--detailed', action='store_true')
parser_show.add_argument('-p', '--pbcopy', action='store_true')
parser_show.add_argument('-s', '--sort', action='store_true')

parser_output = argparse.ArgumentParser(prog='show')
parser_output.add_argument('path', type=str, nargs='?')


def _get_present_dt_string():
    # Note that timestamp could not put in argument.
    now = datetime.now()
    pattern = '%Y%m%d_%H%M%S%f'
    return now.strftime(pattern)


def _get_archive_filename(folder: Text,
                          prefix: Text = 'backup',
                          extension: Text = 'zip'):
    return os.path.join(
        folder,
        prefix + '_' + _get_present_dt_string()[:-4] + '.' + extension, )


def _get_enriched_string(obj: Any, highlight: bool = False) -> Text:
    console = Console()
    with console.capture() as capture:
        console.print(obj, highlight=highlight)
    answer = capture.get()
    return answer


def _write_to_clipboard(string: Text):
    command = 'pbcopy' if os.name == 'posix' else 'clip' if os.name == 'nt' else ''
    process = subprocess.Popen(
        command, env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
    process.communicate(string.encode('utf-8'))


def _execute_commands(adapter: ConfigAdapter, *args):

    if len(args) == 0:
        print(SYNTAX_HELP)
        return

    if args[0] in ['app', 'append']:
        parsed = parser_append.parse_args(args[1:])
        if isinstance(adapter, EntityConfigAdapter):
            for value in parsed.values:
                adapter.append(parsed.alias, 'entities', value)
        elif isinstance(adapter, ProcessLookupError):
            print(MSG_WRONG_ADAPTER.format(type(adapter)))
    elif args[0] == 'pop':
        parsed = parser_pop.parse_args(args[1:])
        if isinstance(adapter, EntityConfigAdapter):
            for value in parsed.values:
                adapter.pop(parsed.alias, 'entities', value)
        elif isinstance(adapter, ProcessLookupError):
            print(MSG_WRONG_ADAPTER.format(type(adapter)))
    elif args[0] == 'sort':
        parsed = parser_sort.parse_args(args[1:])
        if isinstance(adapter, EntityConfigAdapter):
            for alias in parsed.aliases:
                adapter.sort(alias, 'entities', parsed.dedupe)
        elif isinstance(adapter, ProcessLookupError):
            print(MSG_WRONG_ADAPTER.format(type(adapter)))
    elif args[0] == 'mod':
        parsed = parser_mod.parse_args(args[1:])
        if isinstance(adapter, EntityConfigAdapter):
            print(MSG_WRONG_ADAPTER.format(type(adapter)))
        elif isinstance(adapter, ProfileConfigAdapter):
            adapter.mod(parsed.alias, parsed.attrib, parsed.value)
    elif args[0] == 'create':
        parsed = parser_create.parse_args(args[1:])
        for alias in parsed.aliases:
            adapter.create(alias)
    elif args[0] == 'revoke':
        parsed = parser_revoke.parse_args(args[1:])
        for alias in parsed.aliases:
            adapter.revoke(alias)
    elif args[0] == 'reorg':
        parsed = parser_reorg.parse_args(args[1:])
        if parsed.repr is None:
            mapping = {}
        else:
            mapping = literal_eval(parsed.repr)
        adapter.reorg(mapping)
    elif args[0] in ['comp', 'compare']:
        adapter.compare()
    elif args[0] == 'show':
        parsed = parser_show.parse_args(args[1:])
        shown = adapter.show(*parsed.glob, detailed=parsed.detail)
        if parsed.sort:
            sorted_shown = sorted(shown, key=lambda x: repr(x))
        else:
            sorted_shown = shown
        if parsed.pbcopy:
            _write_to_clipboard(_get_enriched_string(sorted_shown))
        else:
            print(sorted_shown)
    elif args[0] in ['clear']:
        adapter.clear()
    elif args[0] in ['update']:
        adapter.update()
    elif args[0] in ['output']:
        parsed = parser_output.parse_args(args[1:])
        adapter.output(parsed.path)
    elif args[0] == 'help':
        print(SYNTAX_HELP)
    else:
        print('Unknown syntax. Use one of the following:')
        print(SYNTAX_HELP)


def interactive_mode(prompt: Text = '>> '):

    input_command = input(prompt)
    while input_command not in ['', 'q', 'quit', 'exit']:
        cmds = shlex.split(input_command)
        if input_command in ['?', 'h', 'help']:
            print('Type "q", "quit", "exit" or ENTER, to leave interactive mode.')
            print(MSG_HOWTO_START)
        elif cmds[0] in ['ent', 'entity']:
            _execute_commands(entities, *cmds[1:])
        elif cmds[0] in ['pro', 'profile']:
            _execute_commands(profiles, *cmds[1:])
        elif cmds[0] in ['backup']:
            zip_folder = os.path.dirname(PATH_ENTITIES)
            zip_filename = _get_archive_filename(zip_folder)
            zip_object = ZipFile(zip_filename, 'w')
            zip_object.write(PATH_ENTITIES, os.path.basename(PATH_ENTITIES))
            zip_object.write(PATH_PROFILES, os.path.basename(PATH_PROFILES))
            zip_object.close()
            logger.info('Zip file has been created. Check {}'.format(zip_filename))
        else:
            print('Unknown syntax.')
            print(MSG_HOWTO_START)
        input_command = input(prompt)


def main():
    args: argparse.Namespace = parser.parse_args()

    if args.method == 'config':
        interactive_mode()
    elif args.method == 'verify':
        pass


if __name__ == '__main__':

    main()
