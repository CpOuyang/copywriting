from typing import Text, List, Dict, Iterable
import os
import pprint
import re

import pandas as pd
import yaml

from cw.utils import dedupe_list

CONFIG_PANDAS_CSV = {'index': True, 'encoding': 'utf8', }
CONFIG_PPRINT = {'compact': True, 'width': 140, 'sort_dicts': False, }

pp = pprint.PrettyPrinter(**CONFIG_PPRINT)

ecoupon_table = pd.read_excel('~/Downloads/(請填寫於此)2021年酷碰新大表.xlsx', sheet_name=1)

single_column_df = ecoupon_table[['優寭案型\n(酷碰大全)\n(必填)']]              # pandas.DataFrame
custom_numpy = ecoupon_table[['優寭案型\n(酷碰大全)\n(必填)']][3:8].to_numpy()  # numpy.ndarray
ecoupon_message = ecoupon_table['優寭案型\n(酷碰大全)\n(必填)']                 # pandas.Series
ecoupon_notice = ecoupon_table['電子優惠券\n注意事項(LINE)\n(必填)']            # pandas.Series
ecoupon_exchanged = ecoupon_table['電子優惠券\n兌換後注意事項(LINE)']           # pandas.Series
for idx, siri in enumerate(ecoupon_exchanged):
    if isinstance(siri, (int, float)):
        ecoupon_exchanged[idx] = repr(siri)
with open(os.path.join('asset', 'merchant_profiles.yml'),
          encoding='utf8') as f:
    merchant_profiles = yaml.safe_load(f)
with open(os.path.join('asset', 'merchant_entities.yml'),
          encoding='utf8') as f:
    merchant_entities = yaml.safe_load(f)


def remove_hyper_tags(string: Text) -> Text:
    # Most contents between <a ...> and </a> are hyoerlinks
    removed = re.sub(r'<a\ .*?</a>', '', string)
    removed = re.sub(r'<.*?>', '', removed)
    removed = re.sub(r'_x000D_', '', removed)

    removed = re.sub(r'\n', ' ', removed)
    removed = re.sub(r'&nbsp;', ' ', removed)

    removed = re.sub(r'&gt;', '>', removed)
    removed = re.sub(r'&amp;', '&', removed)
    return removed


def score_entities(text: Text,
                   lookup_table: List[Dict]) -> List[Dict]:
    answer = []
    default_index = -1
    default_score = 0
    default_alias = ''
    default_matches = []

    for index, item in enumerate(lookup_table):
        score = 0
        matches = []
        alias = item.get('alias')
        patterns = item.get('entities')
        # both keys, and item in that key, are likely to be None if not specified well
        if alias is not None:
            if alias in text:
                score += 10
                matches.append(alias)
        if patterns is not None:
            for pattern in patterns:
                # The behaviors of re.findall and re.finditer are DIFFERENT
                # re.finditer generate iterator of re.Match, which is much likely to re.search
                # try:
                #    text = '大披薩好吃，小披薩更好吃'
                #    pattern = re.compile('(大|小)披薩')
                if pattern is not None:
                    findings = list(re.finditer(pattern, text))
                    if findings:
                        score += 1 * len(findings)
                        matches.append(findings[0].group())
        deduped_matches = dedupe_list(matches)
        dct = {
            'index': index if score else default_index,
            'score': score if score else default_score,
            'alias': alias if score else default_alias,
            'matches': deduped_matches if score else default_matches, }
        answer.append(dct)
    return answer


def lookup_profiles(text: Text,
                    lookup_table: List[Dict]):
    answer = {}
    for profile in lookup_table:
        if text == profile.get('alias'):
            answer = profile
    return answer


def promote_alias(cpn_message: Iterable,
                  cpn_notice: Iterable,
                  cpn_exchanged: Iterable,
                  lookup_table: List[Dict],
                  delimiter: Text = ';;',
                  demo_mode: bool = False):
    answer = []
    def remove_nan(x):
        return '' if x != x else x

    for msg, ntc, xcg in zip(cpn_message, cpn_notice, cpn_exchanged):
        msg = remove_nan(msg)
        ntc = remove_nan(ntc)
        xcg = remove_nan(xcg)
        text = delimiter.join([msg, ntc, xcg])

        scoreboard = score_entities(text, lookup_table)
        leaderboard = sorted(scoreboard,
                             reverse=True,
                             key=lambda x: x.get('score'))
        best = leaderboard[0]
        profile = lookup_profiles(best.get('alias'), merchant_profiles)

        if demo_mode:
            print(msg)
            print(ntc)
            print(xcg)

            pp._width=60

            print('\033[1;32m', end='')
            pp.pprint(best)
            print('\033[0;0m')

            print('\033[1;33m', end='')
            pp.pprint(profile)
            print('\033[0;0m')

            pp._width = CONFIG_PPRINT.get('width')

        # Append the merged dict
        answer.append({**best, **profile})

    return answer


def main():
    promoted = promote_alias(ecoupon_message[:],
                             ecoupon_notice[:],
                             ecoupon_exchanged[:],
                             lookup_table=merchant_entities,
                             demo_mode=True,)

    demo = pd.concat([ecoupon_message,
                      ecoupon_notice,
                      ecoupon_exchanged,
                      pd.DataFrame(promoted), ], axis=1)

    demo = demo.drop(columns='index')
    demo.to_csv('demo.csv', **CONFIG_PANDAS_CSV)
    demo.to_excel('demo.xlsx', )


if __name__ == '__main__':

    main()
