from typing import List


def _freeze(mutable):
    if isinstance(mutable, dict):
        return frozenset((key, _freeze(value)) for key, value in mutable.items())
    elif isinstance(mutable, (set, tuple, list)):
        return tuple(_freeze(value) for value in mutable)
    return mutable


def dedupe_list(lst: List) -> List:
    """Dedupe elements of a list.
    # HACK: Keep original order.
    # HACK: Unhashable items included.
    """
    unique = set()
    answer = [item for item in lst if not (
        _freeze(item) in unique or unique.add(_freeze(item)))]
    return answer
