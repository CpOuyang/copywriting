from typing import Text, Optional

from libs.w3request import client


class CopywritingClient(client):

    def __init__(self,
                 host: Text,
                 port: Optional[int] = None,
                 scheme: Text = 'http',
                 secure: bool = False):
        super().__init__(host, port=port, scheme=scheme, secure=secure)
