from copy import deepcopy
from typing import Text, Tuple, List, Dict, Union, Optional, Literal, Any
import fnmatch
import logging
import os

from rich import print
from rich.console import Console
from rich.padding import Padding
import yaml

from cw.utils import dedupe_list

logging.basicConfig(level=logging.DEBUG)

ALIAS_NAME = 'alias'
PATH_ENTITIES = os.path.abspath(os.path.join('asset', 'merchant_entities.yml'))
PATH_PROFILES = os.path.abspath(os.path.join('asset', 'merchant_profiles.yml'))
MSG_ALIAS_NOT_FOUND = 'Alias not found in configs.'
MSG_ALIAS_NOT_SPECIFIED = 'Alias not specified.'
MSG_ALIAS_EXISTS = 'Alias exists.'
MSG_ATTRIB_WRONG_FORMAT = 'Wrong format of attributes.'
MSG_ATTRIB_NOT_FOUND = 'Attributes not found in configs.'
MSG_ATTRIB_VALUE_NOT_FOUND = 'Value not found in attributes.'
MSG_IMPORT_NULL = 'Empty file imported.'
MSG_IMPORT_WRONG_FORMAT = 'Wrong format of config file. Expect a list of dicstionaries.'
MSG_UNCHANGED = 'No configs were modified. No action would be taken further.'
MSG_CHANGED = 'Configs were modified. Apply self.update() or self.clear() first.'
MSG_COMPLETE_CLEAR = 'Modified configs has been cleared.'
MSG_COMPLETE_UPDATE = 'Modified configs has been updated.'
PALETTE_UNCHANGED = 'dim white'
PALETTE_CHANGED = 'bold green'
PALETTE_DELETED = 'bold red'
PALETTE_NEWADDED = 'bold blue'


def _rich_tags(tag: Text) -> Tuple:
    return (f'[{tag}]', f'[/{tag}]')


def _rich_palette():
    return ' / '.join(['{}Unchanged{}'.format(*_rich_tags(PALETTE_UNCHANGED)),
                       '{}Changed{}'.format(*_rich_tags(PALETTE_CHANGED)),
                       '{}Deleted{}'.format(*_rich_tags(PALETTE_DELETED)),
                       '{}NewAdded{}'.format(*_rich_tags(PALETTE_NEWADDED)), ])


def _move_element_of_list(lst: List[Dict],
                          start: int,
                          endup: int, ) -> List[Dict]:
    """Put dicts in a list in specified or natural (if move=None) order.
    Not the contents of dicts themselves.
    If failed to move (such as out-of-bound index), return a deep-copy.

    Use key augument of sorted function to move.
    For example,
        >>> # Move elemnt indexed 3 to index 6
        >>> sorted(range(10),
        ...        key=lambda x: [0, 0, 0, 2, 1, 1, 1, 3, 3, 3][x])
        [0, 1, 2, 4, 5, 6, 3, 7, 8, 9]
        >>> # Move elemnt indexed 6 to index 3
        >>> sorted(range(10),
        ...        key=lambda x: [0, 0, 0, 2, 2, 2, 1, 3, 3, 3][x])
        [0, 1, 2, 6, 3, 4, 5, 7, 8, 9]
    """
    mapper = []
    lbound, ubound = 0, len(lst)

    if any([start == endup,
            min(start, endup) < lbound,
            ubound < max(start, endup), ]):
        mapper += [0 for i in range(lbound, ubound - 1)]
    else:
        mapper += [0 for i in range(lbound, min(start, endup))]
        if start < endup:
            mapper += [2]
            mapper += [1 for i in range(start, endup)]
        elif endup < start:
            mapper += [2 for i in range(endup, start)]
            mapper += [1]
        mapper += [3 for i in range(max(start, endup), ubound - 1)]
    answer = sorted(lst, key=lambda x: mapper[lst.index(x)])
    return answer


class CustomDumper(yaml.Dumper):
    # HACK: insert blank lines between top-level objects
    # inspired by https://stackoverflow.com/a/44284819/3786245
    def write_line_break(self, data=None):
        super().write_line_break(data)
        if len(self.indents) == 1:
            super().write_line_break()


class ConfigAdapter(object):

    def __init__(self, path: Text):
        self._path: Text = path
        with open(self._path, encoding='utf8') as file:
            self._master: List[Dict] = yaml.load(file, Loader=yaml.FullLoader)
        self._slave = deepcopy(self._master)
        self._alias: Text = ALIAS_NAME
        self._synced: bool = True
        self._console = Console()

        if len(self._master) == 0:
            raise ImportError(MSG_IMPORT_NULL)
        if not isinstance(self._master, list):
            raise ImportError(MSG_IMPORT_WRONG_FORMAT)
        for master in self._master:
            if not isinstance(master, dict):
                raise ImportError(MSG_IMPORT_WRONG_FORMAT)

        self._logger = logging.getLogger(__name__)
        self._logger.debug('Config file imported: {}'.format(self._path))

    @property
    def synced(self):
        return self._synced

    def _check_configs_format(self, configs: List[Dict]) -> Tuple[bool, int, Optional[type]]:
        """Returns (Pass or not, error code, type of errored object)
        """
        if not isinstance(configs, list):
            return False, 1, type(configs)
        for config in configs:
            if not isinstance(config, dict):
                return False, 2, type(config)
        return True, 0, None

    def _check_unused_alias(self, alias: Text) -> bool:
        for config in self._slave:
            occupied = config.get(self._alias, '')
            if alias == occupied:
                return False
        return True

    def _get_alias_config(self,
                          dominator: Literal['master', 'slave'],
                          alias: Text) -> Tuple[Optional[int], Optional[Dict]]:
        if dominator == 'master':
            domin = self._master
        elif dominator == 'slave':
            domin = self._slave
        for idx, config in enumerate(domin):
            if config.get(self._alias) == alias:
                return idx, config
        return None, None

    def _new_config_item(self):
        """For creation purpose.
        """
        raise NotImplementedError

    def _sync_configs(self,
                      dominator: Literal['master', 'slave'],
                      touch: bool,
                      completion_log: Text):
        """Two ways to sync configs cache: update or clear.
        """
        if touch:
            synced = self._master == self._slave
        else:
            synced = self.synced

        if synced:
            self._logger.info(MSG_UNCHANGED)
        else:
            if dominator == 'master':
                self._slave = deepcopy(self._master)
            elif dominator == 'slave':
                self._master = deepcopy(self._slave)
            self._synced = True
            self._logger.info(completion_log)

    def append(self,
               alias: Text,
               attrib: Text,
               value: Text):
        """Append elements to attribute of config named alias.
        """
        _, config = self._get_alias_config('slave', alias)
        if config is None:
            self._logger.warning(MSG_ALIAS_NOT_FOUND)
            return
        if attrib not in config:
            self._logger.warning(MSG_ATTRIB_NOT_FOUND)
            return
        on_board = config.get(attrib)
        if not isinstance(on_board, list):
            self._logger.warning(MSG_ATTRIB_WRONG_FORMAT)
            return
        on_board.append(value)
        self._synced = False

    def pop(self,
            alias: Text,
            attrib: Text,
            value: Text) -> Optional[Dict]:
        """Pop element out of an attribute of config named alias.
        """
        _, config = self._get_alias_config('slave', alias)
        if config is None:
            self._logger.warning(MSG_ALIAS_NOT_FOUND)
            return
        if attrib not in config:
            self._logger.warning(MSG_ATTRIB_NOT_FOUND)
            return
        on_board = config.get(attrib)
        if not isinstance(on_board, list):
            self._logger.warning(MSG_ATTRIB_WRONG_FORMAT)
            return
        if value not in on_board:
            self._logger.warning(MSG_ATTRIB_VALUE_NOT_FOUND)
            return
        popped = on_board.pop(on_board.index(value))
        self._synced = False
        return popped

    def sort(self,
             alias: Text,
             attrib: Text,
             dedupe: bool = False):
        """Sort elements of an attribute of config named alias.
        Dedupe or not.
        """
        _, config = self._get_alias_config('slave', alias)
        if config is None:
            self._logger.warning(MSG_ALIAS_NOT_FOUND)
            return
        if attrib not in config:
            self._logger.warning(MSG_ATTRIB_NOT_FOUND)
            return
        on_board = config.get(attrib)
        if not isinstance(on_board, list):
            self._logger.warning(MSG_ATTRIB_WRONG_FORMAT)
            return
        if dedupe:
            on_board = sorted(set(on_board))
        else:
            on_board = sorted(on_board)
        config[attrib] = on_board
        self._synced = False

    def mod(self,
            alias: Text,
            attrib: Text,
            value: Any):
        """Modify element of an attribute of config named alias.
        """
        _, config = self._get_alias_config('slave', alias)
        if config is None:
            self._logger.warning(MSG_ALIAS_NOT_FOUND)
            return
        if attrib not in config:
            self._logger.warning(MSG_ATTRIB_NOT_FOUND)
            return
        config[attrib] = value
        self._synced = False

    def create(self, alias: Text = ''):
        """Create new config named alias.
        """
        if len(alias) == 0:
            self._logger.warning(MSG_ALIAS_NOT_SPECIFIED)
            return
        _, cfg = self._get_alias_config('slave', alias)
        if cfg is not None:
            self._logger.warning(MSG_ALIAS_EXISTS)
            return
        config = self._new_config_item()
        config[self._alias] = alias
        self._slave.append(config)
        self._synced = False

    def revoke(self, alias: Text = '') -> Dict:
        """Revoke config named alias.
        """
        if len(alias) == 0:
            self._logger.warning(MSG_ALIAS_NOT_SPECIFIED)
            return
        idx, config = self._get_alias_config('slave', alias)
        if config is None:
            self._logger.warning(MSG_ALIAS_NOT_FOUND)
            return
        popped = self._slave.pop(idx)
        self._synced = False
        return popped

    def compare(self):
        """Naive comparison.
        Expect each object being compared a list of dicts.
        """
        base_cfg: List[Dict] = self._master
        comp_cfg: List[Dict] = self._slave
        # Verify configs format
        trans = lambda x: 'list' if x == 1 else 'dict' if x == 2 else ''
        for configs in base_cfg, comp_cfg:
            passed, error_code, error_type = self._check_configs_format(configs)
            if not passed:
                raise TypeError('Type error when verifying format of configs.',
                                f'Expect a {trans(error_code)}, got a {error_type}')

        # Use double braces to be transparent to str.format()
        templatize = lambda x: '{}' + repr(x).replace('{', '{{').replace('}', '}}') + '{}'
        self._console.print(
            'Make comparison of master and slave configs ... :',
            _rich_palette(),)
        for config in base_cfg:
            template_base = templatize(config)
            # Unchanged config
            if config in comp_cfg:
                self._console.print(
                    Padding(template_base.format(*_rich_tags(PALETTE_UNCHANGED)), (0, 4)))
                continue
            alias = config.get(self._alias)
            # Changed config
            if alias in [config.get(self._alias) for config in comp_cfg]:
                new_config = [config for config in comp_cfg
                              if config.get(self._alias) == alias][0]
                template_comp = templatize(new_config)
                self._console.print(
                    Padding(template_comp.format(*_rich_tags(PALETTE_CHANGED)), (0, 4)))
            # Deleted config
            else:
                self._console.print(
                    Padding(template_base.format(*_rich_tags(PALETTE_DELETED)), (0, 4)))
        for config in comp_cfg:
            alias = config.get(self._alias)
            # New config
            if alias not in [config.get(self._alias) for config in base_cfg]:
                template_base = templatize(config)
                self._console.print(
                    Padding(template_base.format(*_rich_tags(PALETTE_NEWADDED)), (0, 4)))

    def reorg(self,
              mapping: Dict = {},
              dominator: Literal['master', 'slave'] = 'slave'):
        """Reorg configs with specified mappings.
        """
        if dominator == 'master':
            domin = self._master
        elif dominator == 'slave':
            domin = self._slave
        if len(mapping) == 0:
            domin = sorted(domin, key=lambda x: x.get(self._alias, ''))
        else:
            for key in mapping:
                val = mapping[key]
                if isinstance(key, int):
                    start = key
                else:
                    start, _ = self._get_alias_config('slave', key)
                if isinstance(val, int):
                    endup = val
                else:
                    endup, _ = self._get_alias_config('slave', val)
                if start is None or endup is None:
                    self._logger.warning(MSG_ALIAS_NOT_FOUND)
                    return
                domin = _move_element_of_list(domin, start, endup)
        if dominator == 'master':
            self._master = domin
        elif dominator == 'slave':
            self._slave = domin
        self._synced = False

    def show(self, *args, detailed: bool = False) -> List[Union[Text, Dict]]:
        """
        Use syntax:

            self.show(merchant-name-1, merchant-name-2, ...)

        to show the most recent contents.
        To be robust, use fnmatch package (wildcards */?)
        to entend the searches rather than regex.
        """
        answer = []
        # Glitch might raised if alias is deuped.
        aliases = [config.get(self._alias) for config in dedupe_list(self._slave)]
        if not args:
            args = aliases
        for slave in self._slave:
            alias = slave.get(self._alias)
            for arg in args:
                if fnmatch.fnmatch(alias, arg):
                    if detailed:
                        answer.append(slave)
                    else:
                        answer.append(alias)
        return answer

    def clear(self, touch: bool = False):
        """Sync configs with original. (Give up changes.)
        """
        self._sync_configs('master', touch, MSG_COMPLETE_CLEAR)

    def update(self, touch: bool = False):
        """Sync configs with recently modified.
        """
        self._sync_configs('slave', touch, MSG_COMPLETE_UPDATE)

    def output(self, path: Text):
        """Always use self._master to output to the file.
        """
        if not self.synced:
            self._logger.warning(MSG_CHANGED)
            return
        # Process path inside method for private variables would not be in argument.
        if path is None:
            path = self._path
        with open(path, 'w', encoding='utf8') as file:
            yaml.dump(self.show(detailed=True),
                      file,
                      allow_unicode=True,
                      sort_keys=False,
                      Dumper=CustomDumper, )


class EntityConfigAdapter(ConfigAdapter):

    def __init__(self, path: Text = PATH_ENTITIES):
        super().__init__(path)

    def _new_config_item(self, alias: Text = '') -> Dict:
        return {
            self._alias: alias,
            'entities': [],
        }


class ProfileConfigAdapter(ConfigAdapter):

    def __init__(self, path: Text = PATH_PROFILES):
        super().__init__(path)

    def _new_config_item(self, alias: Text = '') -> Dict:
        return {
            self._alias: alias,
            'registry': '',
            'type': '',
            'subtype': '',
        }


if __name__ == '__main__':

    from rich import print
    entities = EntityConfigAdapter()
    profiles = ProfileConfigAdapter()

    entities.reorg()

    print(entities._master)
    print(entities._slave)
