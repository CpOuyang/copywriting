from typing import Text, List, Union

from app.ehownet.ehownet_python3 import WordNode, SemanticTypeNode
from cw.client import CopywritingClient

CFG_CKIPTRANS = 'localhost', 3191
CFG_EHOWNET = 'localhost', 3192


class CKIPHandler:

    def __init__(self):
        self._ckiptrans_agent = CopywritingClient(*CFG_CKIPTRANS)
        self._ehownet_agent = CopywritingClient(*CFG_EHOWNET)

    def _post_ckiptrans(self,
                        abbrev: Text,
                        texts: List[Text]):
        path = abbrev
        payload = {'texts': texts}
        return self._ckiptrans_agent.post(path, payload)

    def _post_ehownet(self,
                      theme: Text,
                      relative: Text,
                      text: Text,
                      return_type: Text = 'name'):
        path = '/'.join([theme, relative, text])
        payload = {'return_type': return_type}
        return self._ehownet_agent.post(path, payload)

    def texts_ws(self, texts: List[Text]):
        return self._post_ckiptrans('ws', texts)

    def texts_pos(self, texts: List[Text]):
        return self._post_ckiptrans('pos', texts)

    def texts_ner(self, texts: List[Text]):
        return self._post_ckiptrans('ner', texts)

    def texts_parse(self, texts: List[Text]):
        return self._post_ckiptrans('parse', texts)

    def word_search(self, text: Text,
                    method: Text = 'default',
                    return_type: Text = 'name',):
        path = '/'.join(['word', 'search', text])
        payload = {
            'method': method,
            'return_type': return_type,
        }
        return self._ehownet_agent.post(path, payload)

    def word_synonyms(self, text: Text,
                      return_type: Text = 'name'):
        return self._post_ehownet('word', 'synonyms', text, return_type)

    def word_siblings(self, text: Text,
                      return_type: Text = 'name'):
        return self._post_ehownet('word', 'siblings', text, return_type)

    def word_descendants(self, text: Text,
                         return_type: Text = 'name'):
        return self._post_ehownet('word', 'descendants', text, return_type)

    def word_semantics(self, text: Text,
                       return_type: Text = 'name'):
        return self._post_ehownet('word', 'semantics', text, return_type)

    def semantic_search(self, text: Text,
                        method: Text = 'default',
                        return_type: Text = 'name',):
        path = '/'.join(['semantic', 'search', text])
        payload = {
            'method': method,
            'return_type': return_type,
        }
        return self._ehownet_agent.post(path, payload)

    def semantic_ancestors(self, text: Text,
                           return_type: Text = 'name'):
        return self._post_ehownet('semantic', 'ancestors', text, return_type)

    def semantic_descendants(self, text: Text,
                             return_type: Text = 'name'):
        return self._post_ehownet('semantic', 'descendants', text, return_type)

    def semantic_hypernym(self, text: Text,
                          return_type: Text = 'name'):
        return self._post_ehownet('semantic', 'hypernym', text, return_type)

    def semantic_hyponyms(self, text: Text,
                          return_type: Text = 'name'):
        return self._post_ehownet('semantic', 'hyponyms', text, return_type)

    def semantic_words(self, text: Text,
                       return_type: Text = 'name'):
        return self._post_ehownet('semantic', 'words', text, return_type)


if __name__ == '__main__':

    cc = CKIPHandler()
    print(cc.word_search('開心'))
    print(cc.word_synonyms('開心.Nv,VH.1'))
    print(cc.semantic_words('joyful|喜悅.1'))
