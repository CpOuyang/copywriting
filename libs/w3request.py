from typing import Text, Dict, Optional, Union
import gzip
import json
import logging

from fake_useragent import UserAgent
import requests

from libs.errors import APIError

logging.basicConfig(level=logging.INFO)
useragent = UserAgent()


def _gen_fake_agent(osys: Text = 'macOS', browser: Text = 'Chrome'):
    if (osys, browser) == ('macOS', 'Chrome'):
        return useragent.google
    else:
        return useragent.chrome


class client(object):

    def __init__(self,
                 host: Text,
                 port: Optional[int] = None,
                 scheme: Text = 'http',
                 secure: bool = False):
        self._params = {
            'host': host,
            'port': port,
            'scheme': 'https' if secure else scheme,
        }
        self._logger = logging.getLogger(__name__)
        self._session = requests.Session()
        self._session.headers['User-Agent'] = _gen_fake_agent()
        self._logger.debug('Requests session initialized.')

    def _gen_url(self) -> Text:
        if self._params.get('port') is None:
            pattern = '{scheme}://{host}/{path}'
        else:
            pattern = '{scheme}://{host}:{port}/{path}'
        return pattern.format(**self._params)

    def _gen_resp_as_dict(self, resp: requests.Response) -> Dict:
        if resp.status_code != requests.codes.ok:
            url = self._gen_url()
            raise APIError('Service error with status code {} in {}.'.format(
                resp.status_code, url))
        try:
            return resp.json()
        except json.decoder.JSONDecodeError:
            # No valid json
            return resp.text

    @property
    def port(self):
        return self._params.get('port')

    @port.setter
    def port(self, value):
        self._params['port'] = value

    def get(self,
            path: Text = '') -> Union[Text, Dict]:
        self._params['path'] = path
        url = self._gen_url()
        try:
            resp = self._session.get(url)
        except requests.exceptions.ConnectionError as ce:
            raise ce('Connection error while applying GET method of: {}.'.format(url))
        return self._gen_resp_as_dict(resp)

    def post(self,
             path: Text,
             data: Dict,
             gzip: bool = False) -> Union[Text, Dict]:
        self._params['path'] = path
        url = self._gen_url()
        try:
            if not gzip:
                resp = self._session.post(url, json=data)
            else:
                gz_data = gzip.compress(json.dumps(data).encode("utf-8"))
                resp = self._session.post(
                    url,
                    data=gz_data,
                    headers={
                        "content-type": "application/json",
                        "content-encoding": "gzip",
                    },
                )
            if resp.text:
                return self._gen_resp_as_dict(resp)
            return {}
        except requests.exceptions.ConnectionError as ce:
            raise ce('Connection error when applying POST method of: {}.'.format(url))


if __name__ == '__main__':

    cc = client('httpbin.org')
    print(cc.get('get'))
    print(cc._session.headers)
