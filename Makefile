.PHONY: clean test

help:
	@echo "make"
	@echo "    help"
	@echo "        Show the basic help."
	@echo "    clean"
	@echo "        Remove to-be-removed."

clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f  {} +
	find . -name '.DS_Store' -exec rm -f  {} +
	find . -name '__pycache__' -type d -exec rm -rf  {} +
	find . -name '.ipynb_checkpoints' -type d -exec rm -rf  {} +
	find . -name '.pytest_cache' -type d -exec rm -rf  {} +

test:
	PYTHONPATH=. pytest --cov-report term --cov-report html --cov=cw --log-cli-level INFO -vv --capture=no tests/
	# PYTHONPATH=. pytest --cov-report term --cov-report html --cov=cw --log-cli-level INFO -vv --capture=no tests/cw/test_model.py
